import React from 'react';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

//components
import Navbar from '../ui/components/Navbar'; 

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {

    }

    handlerSubmitLogin = ( event ) => {
        event.preventDefault();

        let user = {
            email : document.getElementById('email').value,
            password : document.getElementById('password').value
        }

        if(user.email == ''){
            Bert.alert('El correo no debe estar vacio.', 'danger', 'fixed-top', 'fa fa-times');
            return false
        }

        if(user.password == ''){
            Bert.alert('La contrasena no debe estar vacia.', 'danger', 'fixed-top', 'fa fa-times');
            return false
        }

        Meteor.loginWithPassword(user.email, user.password, function (err, res) {
            if (err) {
                if (err.message === 'User not found [403]') {
                    Bert.alert('Usuario no encontrado', 'danger', 'fixed-top', 'fa fa-times');
                } else {
                    Bert.alert('Usuario incorrecto o contrasena', 'danger', 'fixed-top', 'fa fa-times');
                }
            } else {
                FlowRouter.go('App.home');
            }
        });
    }

    render() {
        return (
            <div className="text-center">
                
                <Navbar />

                <br></br>

                <main className="form-signin">
                    <form onSubmit={this.handlerSubmitLogin}>
                        <h1 className="h3 mb-3 fw-normal">Login</h1>

                        <div className="form-floating">
                            <input type="email" className="form-control" id="email" placeholder="name@example.com" />
                            <label htmlFor="floatingInput">Email address</label>
                        </div>
                        
                        <div className="form-floating">
                            <input type="password" className="form-control" id="password" placeholder="Password" />
                            <label htmlFor="floatingPassword">Password</label>
                        </div>
                      
                        <br></br>
                        <button className="w-100 btn btn-lg btn-primary" type="submit">Send</button>
                        <p className="mt-5 mb-3 text-muted">© tutorial meteor+react - { new Date().getFullYear() }</p>
                    </form>
                </main>
            </div>
        );
    }
}