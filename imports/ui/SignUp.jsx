import React from 'react';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

//components
import Navbar from './components/Navbar'; 

export default class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {

    }

    handlerSubmitSignUp = ( event ) => {
        event.preventDefault();

        let form = event.currentTarget;

        let new_user = {
            email : document.getElementById('email').value,
            password : document.getElementById('password').value
        }

        if(new_user.email == ''){
            Bert.alert('El correo no debe estar vacio.', 'danger', 'fixed-top', 'fa fa-times');
            return false
        }

        if(new_user.password == ''){
            Bert.alert('La contrasena no debe estar vacia.', 'danger', 'fixed-top', 'fa fa-times');
            return false
        }

        Meteor.call('create.user' , new_user ,  ( error , response ) => {
            if(!error){
                Bert.alert(`Se creo la cuenta correctamente.`, 'success', 'fixed-top', 'fa fa-check');
                form.reset();
                FlowRouter.go('App.login');
            }else{
                Bert.alert('Error, intenta de nuevo ' + error.error, 'danger', 'fixed-top', 'fa fa-times');
            }
        });
    }

    render() {
        return (
            <div className="text-center">

                <Navbar />

                <br></br>

                <main className="form-signin">
                    <form onSubmit={this.handlerSubmitSignUp}>
                        <h1 className="h3 mb-3 fw-normal">Sign up</h1>
                        <div className="form-floating">
                            <input type="email" className="form-control" id="email" placeholder="name@example.com" />
                            <label htmlFor="floatingInput">Email address</label>
                        </div>
                        <div className="form-floating">
                            <input type="password" className="form-control" id="password" placeholder="Password" />
                            <label htmlFor="floatingPassword">Password</label>
                        </div>

                        <br></br>
                        
                        <button className="w-100 btn btn-lg btn-primary" type="submit">Send</button>
                        <p className="mt-5 mb-3 text-muted">© tutorial meteor+react - { new Date().getFullYear() }</p>
                    </form>
                </main>

            </div>
        );
    }
}