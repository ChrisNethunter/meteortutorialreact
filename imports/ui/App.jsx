import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Tasks } from '../api/Tasks/Tasks';

//components
import Navbar from '../ui/components/Navbar'; 

class App extends React.Component {

	state = { 
		tasks : [],
		showFormCreate : false ,
		showFormEdit : false,
		dataEditTask : {},
		idTaskEdit : '',
		titleEditTask : '',
		descriptionEditTask :'',
	}

	constructor(props) {
		super(props);
		
	}

	componentDidMount() {

	}

	static getDerivedStateFromProps ( nextProps , prevState ) {
        let tasks = nextProps.tasks
		return {
			tasks : tasks,
		}
    }

	handlerSubmitCreateTask = ( event ) => {
		event.preventDefault();
		
		let form = event.currentTarget;
		let task = {
			user: Meteor.userId(),
			title : document.getElementById('title-task').value,
			description : document.getElementById('description-task').value
		}

		Meteor.call('create.task' , task , (error , response ) => {
			if(!error){
				Bert.alert(`Se creo la tarea correctamente.`, 'success', 'fixed-top', 'fa fa-check');
                form.reset();
				this.toogleForms();
			}else{
                Bert.alert('Error, intenta de nuevo ' + error.error, 'danger', 'fixed-top', 'fa fa-times');
            }
		});
	}

	handlerSubmitEditTask = ( event ) => {
		event.preventDefault();
		let form = event.currentTarget;
		let task = {
			_id : this.state.idTaskEdit,
			title : document.getElementById('title-task-edit').value,
			description : document.getElementById('description-task-edit').value
		}

		Meteor.call('update.task' , task , (error , response ) => {
			if(!error){
				Bert.alert(`Se edito la tarea correctamente.`, 'success', 'fixed-top', 'fa fa-check');
                form.reset();
				this.toogleForms();
			}else{
                Bert.alert('Error, intenta de nuevo ' + error.error, 'danger', 'fixed-top', 'fa fa-times');
            }
		});
	}

	handlerClickBtnCreateForm = ( event ) => {
		event.preventDefault();
		this.setState({
			showFormCreate : true
		})
	}

	handlerClickBtnCancelForm = ( event ) => {
		event.preventDefault();
		this.toogleForms();
	}
	
	handlerClickDeleteTask = ( event ) => {
		event.preventDefault();
		Meteor.call('delete.task' , event.currentTarget.id , (error , response ) => {
			if(!error){
				Bert.alert(`Se elimino la tarea correctamente.`, 'success', 'fixed-top', 'fa fa-check');
			}else{
                Bert.alert('Error, intenta de nuevo ' + error.error, 'danger', 'fixed-top', 'fa fa-times');
            }
		});
	}
	
	toogleForms(){
		this.setState({
			showFormCreate : false,
			showFormEdit : false
		})
	}

	handlerClickEditTask = ( event ) => {
		event.preventDefault();

		let find_data_task = Tasks.findOne({ _id : event.currentTarget.id });
		if(!find_data_task){
			return false
		}

		this.setState({
			showFormEdit : true,
			idTaskEdit : event.currentTarget.id,
			titleEditTask : find_data_task.title,
			descriptionEditTask : find_data_task.description
		});
	}

	render() {
		return (
			<div>
				<Navbar />
				
				{
					this.state.showFormCreate ? 
						<main className="form-signin">
							<form onSubmit={this.handlerSubmitCreateTask}>
								<h1 className="h3 mb-3 fw-normal">Crear tarea</h1>

								<div className="form-floating">
									<input type="text" className="form-control" id="title-task" placeholder="name@example.com" />
									<label htmlFor="floatingInput">Titulo</label>
								</div>
								
								<div className="form-floating">
									<input type="text" className="form-control" id="description-task" placeholder="Password" />
									<label htmlFor="floatingPassword">Descripcion</label>
								</div>
							
								<br></br>

								<div className="row">
									<div className="col">
										<button className="w-100 btn btn-lg btn-primary" type="submit">Enviar</button>
									</div>
									<div className="col">
										<button onClick={this.handlerClickBtnCancelForm} className="w-100 btn btn-lg btn-danger" type="submit">Cancelar</button>
									</div>
								</div>
							</form>
						</main>
					:
					<div className="content">
						
						{
							Meteor.userId() ? 
								<div className="bg-light p-5 rounded">
									<h1>Lista de tareas</h1>
									<p className="lead">
										Tutorial Meteor + React 
									</p>
									<a 
										className="btn btn-lg btn-primary" 
										href="#" 
										onClick={this.handlerClickBtnCreateForm}>
										Crear tarea
									</a>
								</div>
							:
							<div className="bg-light p-5 rounded">
								<h1>Debes iniciar sesion</h1>
								<p className="lead">
									Tutorial Meteor + React 
								</p>
								<a className="btn btn-lg btn-primary" href="/login" role="button">Login</a>
							</div>
							
						}


						{
							this.state.showFormEdit ? 
								<main className="form-signin">
									<form onSubmit={this.handlerSubmitEditTask}>
										<h1 className="h3 mb-3 fw-normal">Editar tarea</h1>

										<div className="form-floating">
											<input type="text" className="form-control" id="title-task-edit" defaultValue={this.state.titleEditTask}/>
											<label htmlFor="floatingInput">Titulo</label>
										</div>
										
										<div className="form-floating">
											<input type="text" className="form-control" id="description-task-edit" defaultValue={this.state.descriptionEditTask}/>
											<label htmlFor="floatingPassword">Descripcion</label>
										</div>
									
										<br></br>

										<div className="row">
											<div className="col">
												<button className="w-100 btn btn-lg btn-primary" type="submit">Guardar</button>
											</div>
											<div className="col">
												<button onClick={this.handlerClickBtnCancelForm} className="w-100 btn btn-lg btn-danger" type="submit">Cancelar</button>
											</div>
										</div>
									</form>
								</main>

							:

							<div className="bd-example">
								<table className="table table-striped">
									<thead>
										<tr>
										<th scope="col">Titulo</th>
										<th scope="col">Descripcion</th>
										<th scope="col">Opciones</th>
										</tr>
									</thead>
									<tbody>
										{
											this.state.tasks.map( ( task , key )  =>
												<tr key={key}>
													<td>{task.title}</td>
													<td>{task.description}</td>
													<td className="row">
														<div className="col">
															<button id={task._id} className="btn btn-warning" onClick={this.handlerClickEditTask} >Editar</button>
														</div>
														<div className="col">
															<button id={task._id} className="btn btn-danger" onClick={this.handlerClickDeleteTask} >Eliminar</button>
														</div>
														
													</td>
												</tr>
											)
										}
									</tbody>
								</table>
							</div>
						}
						
					</div>
				}

			</div>
		);
	}
}

export default withTracker(() => {
    
    const handlerSuscriptionLaundry = Meteor.subscribe('all.tasks' , Meteor.userId() ? Meteor.userId()  : ""  );
    let loading = !handlerSuscriptionLaundry.ready();
    let tasks = Tasks.find({ user : Meteor.userId() ? Meteor.userId()  : "" }).fetch();

    return {
        loading,
        tasks,
    };
    
})(App);
